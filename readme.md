# Simple Flask site

Site simulation (list and detail view with images) written in Python using Flask, 
SQLAlchemy and PostgreSQL (or SQLite3) database

# Requirements:
```
Python>=3.6.1
Packages:
    Flask==0.12.2
    Flask-SQLAlchemy==2.2
    faker==0.8.4
    psycopg2==2.7.1
Database:
    PostgreSQL 9.6
    or
    SQLite 3.18.0
```

# Initialization
Before 1st run, database should be initialized by running **init.py** module. It
will create table "items" and upload test data 
```
python init.py
```

# Run site
```
python app.py
```